"""
The definition of a Player
"""

from four import events

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class player(object):
    '''
    Object that attaches to a Game. Receives Events from the Game and
    sends moves to it.
    Most likely not used directly but by subclassing it and overriding on_*
    methods.
    '''
    def __init__(self, name):
        '''
        Initianlizes a Player and ensures that Events are handled.
        :param name: The Name of the Player
        '''
        self.name = name
        self.game = None
        self.end_handlers = []
        self.turn_handlers = []
        self.add_end_game_handler(self.on_end)
        self.add_turn_handler(self.on_turn)

    def play(self, game):
        '''
        Participate in a Game
        :param game:
        '''
        self.game = game
        self.game.add_player(self)

    def handle_event(self, event):
        '''
        Handle incoming events and call the appropriate local handlers
        :param event:
        '''
        if isinstance(event, events.turn_event):
            for handler in self.turn_handlers:
                handler(event)
        if isinstance(event, events.end_event):
            for handler in self.end_handlers:
                handler(event)

    def add_end_game_handler(self, handler):
        '''
        Add Event handler for end of game events
        :param handler:
        '''
        self.end_handlers.append(handler)

    def add_turn_handler(self, handler):
        '''
        Add event handler for change of turns events
        :param handler:
        '''
        self.turn_handlers.append(handler)

    def on_turn(self, event):
        '''
        Stub for an turn event handler
        :param event:
        '''

    def on_end(self, event):
        '''
        Stub for a end game event handler
        :param event:
        '''
