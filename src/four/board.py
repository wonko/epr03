"""
The Playing Field of the Game
"""
from four import winning_pattern as wp

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class board():
    '''
    board provides the state of the game board and related functions

    The board is a rectangular map of squares that may have 3 states:
        - not filled yet (0)
        - played by player 1 (1)
        - played by player 2 (2)
    The squares are indexed from left to right / from bottom to top at index 0.
    '''

    def __init__(self, width, height):
        '''
        initialise an empty game board
        '''
        self.width = width
        self.height = height
        self.state = [[0] * width for i in range(height)]
        self.winning_patterns = wp.winning_pattern()

    def __str__(self):
        '''
        returns a printable ascii picture of the auctual board state
        '''
        # field symbols contains the symbols for the 3 states a
        # field can have (empty, player1, player2)
        # to be indexed by the state of the respect field
        field_symbols = ' XO'

        result = '  ' + '   '.join(self.get_col_headers()) + '\n'
        result += '+-' + '-+-'.join(['-'] * self.width) + '-+\n'
        for row in reversed(self.get_rows()):
            symbols = map(lambda s: field_symbols[s], row)
            result += '| ' + ' | '.join(symbols) + ' |\n'
            result += '+-' + '-+-'.join(['-'] * self.width) + '-+\n'
        return result

    def get_col_headers(self):
        colheaders  = '123456789'
        colheaders += 'abcdefghijklmnopstuvwxyz'
        colheaders += 'ABCDEFGHIJKLMNOPSTUVWXYZ'
        return colheaders[:self.width]

    def get_state(self):
        '''
        return state matrix (row by row)
        '''
        return self.state

    def get_rows(self):
        '''
        return state matrix (row by row)
        '''
        return self.state

    def get_cols(self):
        '''
        return transposed state matrix (col by col)
        '''
        return [list(map(lambda row: row[i],
                         self.state)) for i in range(self.width)]

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def is_position_valid(self, x, y):
        '''
        return True for fields within the game board
        '''
        return (0 <= x < self.width) and (0 <= y < self.height)

    def get_position(self, x, y):
        '''
        return state matrix element at specific position
        (0, 0: lower left corner)
        '''
        if self.is_position_valid(x, y):
            try:
                return self.state[x][y]
            except:
                return -1
        else:
            return -1

    def get_playable_cols(self):
        '''
        return the numbers of cols, that are not full yet
        '''
        # enumerate the colums and keep those that
        # still contain unplayed fields (0)
        playable_cols = filter(lambda e: e[1].count(0) > 0,
                               enumerate(self.get_cols()))
        # cut off the colum content, keep their numbers
        playable_col_numbers = list(map(lambda e: e[0], playable_cols))
        return playable_col_numbers

    def get_playable_col_headers(self):
        return list(map(lambda c: self.get_col_headers()[c],
                        self.get_playable_cols()))

    def col_input_to_number(self, colinput):
        if isinstance(colinput, int):
            return colinput
        elif isinstance(colinput, str):
            # disable case sensivity if not required
            if self.get_col_headers().lower() == self.get_col_headers():
                colinput = colinput.lower()
#            print(repr(colinput), self.get_col_headers().index(colinput))
            return self.get_col_headers().index(colinput)
        else:
            raise ValueError

    def move(self, colinput, player):
        '''
        play a move and return wheather that was possible or not

        if the move ist possible, it is carried out
        otherwise the state matrix is not changed

        col is expeccted as the char displayed as column header
        ToDo: accept get_playable_col_headersplayer objects directly to be able to
              handle an arbitrary number of players on the board
        '''
        try:
            col = self.col_input_to_number(colinput)
        except ValueError:
            return False

        if col not in self.get_playable_cols():
            # the move cannot be done since this col is not available
            return False
        # do the move (obtain first free field, insert player there)
        lowest_free_field = self.get_cols()[col].index(0)
        self.state[lowest_free_field][col] = player
        return True

    def finished(self):
        '''
        return wheater the game is finished by a winning pattern
        '''
        return self.winning_patterns.is_game_finished(self)
