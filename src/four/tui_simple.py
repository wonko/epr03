"""
Crude TUI used for development
"""
from four.game import game
from four.player import player
from four.bots import stupobot
__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class tui_player(player):
    '''
    A Player that interacts via the text console
    '''

    def __init__(self, name):
        '''
        Create a Player
        :param name: Use name for Player
        '''
        super().__init__(name)

    def on_turn(self, event):
        '''
        Called by the Game for a change of turns event.
        :param event:
        '''
        print(self.game.board)
        while event.player is self:
            try:
                m = input(self.name + ' Its your turn, which col do you ' +
                          'want to play? (q: quit, r: reset): ')
            except KeyboardInterrupt:
                # replace the next line by some cleanup code
                raise KeyboardInterrupt
            # validate
            if m == 'q':
                # cleanup + leave the game
                pass
            if m == 'r':
                # cleanup + restart the game
                pass
            try:
                col_raw = int(m)
            except:
                print('unable to understand which col was to be played, ' +
                      'please try again.')
                continue
            # either verfy m to fit the board state or
            # better repeat asking until a move is done
            # (hint: board.move returns the success state)
            self.game.move(self, col_raw)

        def on_end(self, event):
            '''
            Called by the Game for a End of Game event.
            :param event:
            '''
            print(self.game.board)
            if event.winner:
                print ('Player ' + event.winner.name + 'won!')
            else:
                print (event.message)


def tui():
    '''
    Start the TUI
    '''
    g = game()
    p1 = tui_player('P1')
    p1.play(g)

    p2 = stupobot('p2')
    p2.play(g)
    g.startgame()
