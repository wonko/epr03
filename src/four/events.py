"""
Event types that are sent out by the Game.
"""

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class __event(object):
    '''
    Base class for all events
    '''


class turn_event(__event):
    '''
    A change of turn event
    '''
    def __init__(self, player):
        self.player = player


class end_event(__event):
    '''
    A End of Game event
    '''
    def __init__(self, winner=None, message=None):
        self.winner = winner
        self.message = message


class game_end_event(__event):
    pass
