"""
Bots to play against
"""

from four.player import player
from random import randint

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class stupobot(player):
    '''
    A really stupid bot. Just randomly throws pieces in.
    '''

    def on_turn(self, event):
        if event.player is self:
            cols = self.game.board.get_playable_cols()
            col = cols[randint(0, len(cols) - 1)]
            self.game.move(self, col)

    def __init__(self, name):
        super().__init__(name)
