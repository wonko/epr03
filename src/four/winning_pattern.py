"""
For finding out whether or not a Piece on a Board is in a Pattern that
is a win.
"""
from collections import namedtuple
import itertools as it

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


position = namedtuple('field', ['x', 'y'])


class winning_pattern():
    '''
    manages the winning patterns and checking them against a board state
    '''
    def __init__(self):
        '''
        provides a complete set of fields a player must own to win
        '''
        # define the role model for a configuration of fields required to win
        first_winning_pattern = [position(0, 0),
                                 position(1, 0),
                                 position(2, 0),
                                 position(2, 1)]

        # build list of winning patterns, start off with the role model
        self.winning_patterns = [first_winning_pattern]
        # add the horizontal reflection of the first 1
        self.winning_patterns.extend(list(map(self.mirror_hor,
                                              self.winning_patterns)))
        # add the vertical reflection of the first 2
        self.winning_patterns.extend(list(map(self.mirror_vert,
                                              self.winning_patterns)))
        # add the main diagonal reflection of the first 4
        self.winning_patterns.extend(list(map(self.mirror_diag,
                                              self.winning_patterns)))

    def mirror_hor(self, pattern):
        '''
        return the provided pattern mirrored horizontally
        '''
        return list(map(lambda p: position(-p.x, p.y), pattern))

    def mirror_vert(self, pattern):
        '''
        return the provided pattern mirrored vertically
        '''
        return list(map(lambda p: position(p.x, -p.y), pattern))

    def mirror_diag(self, pattern):
        '''
        return the provided pattern mirrored at the main diagonal
        '''
        return list(map(lambda p: position(p.y, p.x), pattern))

    def x_values(self, pattern):
        '''
        return all x coordinated of the provided pattern
        '''
        return list(map(lambda p: p.x, pattern))

    def y_values(self, pattern):
        '''
        return all y coordinated of the provided pattern
        '''
        return list(map(lambda p: p.y, pattern))

    def is_game_finished(self, board):
        '''
        return weather a winning pattern starts at the provided position
        '''
        # loop over all fields of the board and all winning patterns
        for x, y, wp in it.product(range(board.get_width()),
                                   range(board.get_height()),
                                   self.winning_patterns):
            # shift the actual winning pattern to the actual tested field
            wp = list(map(lambda p: position(p[0]+x, p[1]+y), wp))
            # obtain the owners the these fields
            field_owners = list(map(lambda p: board.get_position(*p), wp))
            # check weather -1 (fields next to the board)
            # or 0 (fields within the board but not played (yet))
            # are in die actual tested pattern
            # in that case skip to the next check
            if -1 in field_owners or 0 in field_owners:
                continue
            # if all tested fields are played, return weather they are
            # played by the same player
            if len(set(field_owners)) == 1:
#                print('player %s won' % str(field_owners[0]))
                return True
        # in no tests a winning condition was found, return false
        return False

    def test(self):
        import four.board
        for wp in self.winning_patterns:
            b = four.board.board(6, 5)
            for x, y in wp:
                b.state[x+2][y+2] = 1
            print(b)
            print(self.is_game_finished(b))



