"""
The Game is the central hub to which each Player connects.
"""
from four import events
from four.board import board

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class game(object):
    '''
    Game Object to Orchestrate playing a Game.
    '''

    class players():
        '''
        All Players that Play a Game meet here.
        '''
        players = []
        max_players = 2
        current = None

        def is_current(self, player):
            '''
            Tells wether its the given Players turn.
            :param player: Is it This players Turn?
            '''
            return player is self.players[self.current]

        def append(self, player):
            '''
            Add a Player to the Game.
            :param player: Player to be added.
            '''
            if len(self.players) < self.max_players:
                self.players.append(player)

        def next(self):
            '''
            Return the next Player who's turn it is.
            '''
            try:
                nextplayer = self.current + 1
                player = self.players[nextplayer]
                self.current = nextplayer
            except:
                self.current = 0
                player = self.players[self.current]
            return player

    players = players()
    event_handlers = []
    game_started = False
    board = None

    def add_player(self, player):
        '''
        Add a Player to the Game.
        :param player: The Player
        '''
        self.players.append(player)
        self.add_handler(player.handle_event)

    def add_handler(self, handler):
        '''
        Add an Event Handler to the Game.
        :param handler: Handler receives all Events sent out by the Game.
        '''
        self.event_handlers.append(handler)

    def startgame(self):
        '''
        Start the Game by creating a new Board.
        '''
        self.board = board(6, 8)
        self.game_started = True
        self.__nexturn()

    def move(self, player, column):
        '''
        Make a move.
        :param player: Player that wants to move.
        :param column: The column header into which the player puts its stone.
        '''
        if not self.players.is_current(player):
            pass  # Exception or Message?

        moved = self.board.move(column, self.players.current + 1)
        if moved:
            if self.board.finished():
                self.game_started = False
                self.__game_won(player)
            else:
                playable_cols = self.board.get_playable_cols()
                if not playable_cols:
                    self.game_started = False
                    self.__game_over()
                else:
                    self.__nexturn()
        return moved

    def __nexturn(self):
        turnevent = events.turn_event(self.players.next())
        for handler in self.event_handlers:
            handler(turnevent)

    def __game_won(self, player):
        endevent = events.end_event(winner=player, message="won the Game!")
        for handler in self.event_handlers:
            handler(endevent)

    def __game_over(self):
        endevent = events.end_event(message="No Playable Columns left. " +
                                            "Nobody won!")
        for handler in self.event_handlers:
            handler(endevent)
