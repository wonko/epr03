""""
Unit tests
"""
import unittest
from four.board import board

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class TestBoard(unittest.TestCase):

    def setUp(self):
        self.board = board(6, 8)

    def tearDown(self):
        pass

    def test_impossible_full(self):
        for m in range(1, 9):
            self.assertTrue(self.board.move(0, 1))
        self.assertFalse(self.board.move(0, 1))

    def test_all_cols_work(self):
        for m in range(0,6):
            self.assertTrue(self.board.move(m, 1))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
