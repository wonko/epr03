"""
TUI that uses curses
"""
import curses
import sys
from four.game import game
from four.player import player
from four.bots import stupobot
__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"


class curses_player(player):
    '''
    A player that acts via an ncurses text ui.
    '''

    def __init__(self, name):
        '''
        Create Player and init ncures.
        :param name: name of Player
        '''
        super().__init__(name)
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.stdscr.addstr(0, 0, "Hit 'q' to quit or 'r' to restart Game!")
        self.stdscr.refresh()

    def on_turn(self, event):
        '''
        Called by the Game for a change of turn events.
        (re-) draws the ui
        :param event: The Event from the Game
        '''
        self.stdscr.addstr(1, 0, self.game.board.__str__())
        self.stdscr.refresh()
        while event.player is self:
            key = ''
            self.stdscr.addstr(self.name + ', Its your turn. Which col' +
                                           ' do you want to play?: ')
            self.stdscr.refresh()
            while True:
                try:
                    key = chr(self.stdscr.getch())
                except KeyboardInterrupt:
                    # handle [CTRL]+C event (cleanup and quit)
                    curses.endwin()
                    sys.exit()
                if key == 'q':
                    curses.endwin()
                    sys.exit()
                elif key == 'r':
                    self.game.startgame()
                elif self.game.move(self, key):
                    break

    def on_end(self, event):
        '''
        Called by the Game for an end of game event.
        :param event:
        '''
        self.stdscr.addstr(1, 0, self.game.board.__str__())

        if event.winner:
            self.stdscr.addstr('Player ' + event.winner.name + ' won!')
        else:
            self.stdscr.addstr(event.message)
        self.stdscr.refresh()


def tui():
    '''
    Start the UI
    '''

    g = game()
    try:
        p1_name = input('Enter Player Name: ')
    except KeyboardInterrupt:
        # handle [CTRL]+C event (add newline and quit)
        print()
        sys.exit()
    bot = input('Add a second Player? [y]/n ?')
    if bot not in ['y', 'Y', '']:
        p2 = stupobot('Bot')
    else:
        try:
            name = input('Enter Player Name: ')
        except KeyboardInterrupt:
            # handle [CTRL]+C event (add newline and quit)
            print()
            sys.exit()
        p2 = curses_player(name)
    p1 = curses_player(p1_name)
    p1.play(g)
    p2.play(g)
    g.startgame()
