#!/usr/bin/env python3
""""
Starts the UI of the Game.
"""

from four import tui_curses as tui

__author__ = "6298324: Manuel Dittrich, 6497028: Christian Prause"
__copyright__ = "Copyright 2016 - Manuel Dittrich, Christian Prause"
__licenese__ = "GPL V3"
__email__ = "wonko@stud.uni-frankfurt.de, dachdecker2@web.de"

if __name__ == '__main__':
    tui.tui()
